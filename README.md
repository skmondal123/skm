#!/usr/bin/perl -w
print "Enter the nucleotide file:";
$abc=<STDIN>;
chomp $abc;
open(file1,$abc);
@xyz=<file1>;
#chomp(@xyz=<file1>);
close file1;
$k=scalar(@xyz);
print "\n\n\n", $k, "\n\n\n";
$def='N2.out';
$l=0;$tot=0;
@arr=qw/gca gcc gcg gcu ugc ugu gac gau gaa gag uuc uuu gga ggc ggg ggu cac cau aua auc auu aaa aag uua uug cua cuc cug cuu aug aac aau cca ccc ccg ccu caa cag aga agg cga cgc cgg cgu agc agu uca ucc ucg ucu aca acc acg acu gua guc gug guu ugg uac uau/;

$xyz=join("",@xyz);
@xyz=split(/>/,$xyz);
shift(@xyz);
open(file2,">$def");
foreach (@xyz)
{
	foreach$a(@arr)
	{
		$hash{$a}=0;
	}
	@abc=split(/\n/,$_);
	$name=shift@abc;
	$seq=join("",@abc);
	$seq=~s/\n//gs;
	print file2 ">$name","\n";
	$l=length $seq;
        for($i=0;$i < $l-2;$i=$i+3)
        {
                $code=substr($seq,$i,3);
		$code=~s/t/u/g;
		$hash{$code}++;
	}
	foreach(keys%hash)
	{
		$tot+=$hash{$_};
	}
	print  $tot,"\t", "\t\t",$hash{aug}, "\n\n";
	foreach(@arr)
	{
		$hash{$_}/=$tot if $tot !=0 && exists $hash{$_};
		$hash{$_}*=1000 if exists $hash{$_};
	}
	$rgca=$hash{gca}*4/($hash{gca}+$hash{gcc}+$hash{gcg}+$hash{gcu}) if exists $hash{gca};
        $rgcc=$hash{gcc}*4/($hash{gca}+$hash{gcc}+$hash{gcg}+$hash{gcu}) if $hash{gcc}!=0;
        $rgcg=$hash{gcg}*4/($hash{gca}+$hash{gcc}+$hash{gcg}+$hash{gcu}) if $hash{gcg}!=0;
        $rgcu=$hash{gcu}*4/($hash{gca}+$hash{gcc}+$hash{gcg}+$hash{gcu}) if $hash{gcu}!=0;
        $rugc=$hash{ugc}*2/($hash{ugc}+$hash{ugu}) if $hash{ugc}!=0;
        $rugu=$hash{ugu}*2/($hash{ugc}+$hash{ugu}) if $hash{ugu}!=0;
        $rgac=$hash{gac}*2/($hash{gac}+$hash{gau}) if $hash{gac}!=0;
        $rgau=$hash{gau}*2/($hash{gac}+$hash{gau}) if $hash{gau}!=0;
        $rgaa=$hash{gaa}*2/($hash{gaa}+$hash{gag}) if $hash{gaa}!=0;
        $rgag=$hash{gag}*2/($hash{gaa}+$hash{gag}) if $hash{gag}!=0;
        $ruuc=$hash{uuc}*2/($hash{uuc}+$hash{uuu})  if $hash{uuc}!=0;
        $ruuu=$hash{uuu}*2/($hash{uuc}+$hash{uuu})  if $hash{uuu}!=0;
        $rgga=$hash{gga}*4/($hash{gga}+$hash{ggc}+$hash{ggg}+$hash{ggu}) if $hash{gga}!=0;
        $rggc=$hash{ggc}*4/($hash{gga}+$hash{ggc}+$hash{ggg}+$hash{ggu}) if $hash{ggc}!=0;
        $rggg=$hash{ggg}*4/($hash{gga}+$hash{ggc}+$hash{ggg}+$hash{ggu}) if $hash{ggg}!=0;
        $rggu=$hash{ggu}*4/($hash{gga}+$hash{ggc}+$hash{ggg}+$hash{ggu}) if $hash{ggu}!=0;
        $rcac=$hash{cac}*2/($hash{cac}+$hash{cau})  if exists $hash{cac};
        $rcau=$hash{cau}*2/($hash{cac}+$hash{cau}) if $hash{cau}!=0;
        $raua=$hash{aua}*3/($hash{aua}+$hash{auc}+$hash{auu}) if $hash{aua}!=0;
        $rauc=$hash{auc}*3/($hash{aua}+$hash{auc}+$hash{auu}) if $hash{auc}!=0;
        $rauu=$hash{auu}*3/($hash{aua}+$hash{auc}+$hash{auu}) if $hash{auu}!=0;
        $raaa=$hash{aaa}*2/($hash{aaa}+$hash{aag}) if $hash{aaa}!=0;
        $raag=$hash{aag}*2/($hash{aaa}+$hash{aag}) if $hash{aag}!=0;
        $ruua=$hash{uua}*6/($hash{uua}+$hash{uug}+$hash{cua}+$hash{cuc}+$hash{cug}+$hash{cuu}) if $hash{uua}!=0;
        $ruug=$hash{uug}*6/($hash{uua}+$hash{uug}+$hash{cua}+$hash{cuc}+$hash{cug}+$hash{cuu}) if $hash{uug}!=0;
        $rcua=$hash{cua}*6/($hash{uua}+$hash{uug}+$hash{cua}+$hash{cuc}+$hash{cug}+$hash{cuu}) if $hash{cua}!=0;
        $rcuc=$hash{cuc}*6/($hash{uua}+$hash{uug}+$hash{cua}+$hash{cuc}+$hash{cug}+$hash{cuu}) if $hash{cuc}!=0;
        $rcug=$hash{cug}*6/($hash{uua}+$hash{uug}+$hash{cua}+$hash{cuc}+$hash{cug}+$hash{cuu}) if $hash{cug}!=0;
        $rcuu=$hash{cuu}*6/($hash{uua}+$hash{uug}+$hash{cua}+$hash{cuc}+$hash{cug}+$hash{cuu}) if $hash{cuu}!=0;
        $raug=1.0;
        $raac=$hash{aac}*2/($hash{aac}+$hash{aau})  if $hash{aac}!=0;
        $raau=$hash{aau}*2/($hash{aac}+$hash{aau})  if $hash{aau}!=0;
        $rcca=$hash{cca}*4/($hash{cca}+$hash{ccc}+$hash{ccg}+$hash{ccu}) if $hash{cca}!=0;
        $rccc=$hash{ccc}*4/($hash{cca}+$hash{ccc}+$hash{ccg}+$hash{ccu}) if $hash{ccc}!=0;
        $rccg=$hash{ccg}*4/($hash{cca}+$hash{ccc}+$hash{ccg}+$hash{ccu}) if $hash{ccg}!=0;
        $rccu=$hash{ccu}*4/($hash{cca}+$hash{ccc}+$hash{ccg}+$hash{ccu}) if $hash{ccu}!=0;
        $rcaa=$hash{caa}*2/($hash{caa}+$hash{cag})  if $hash{caa}!=0;
        $rcag=$hash{cag}*2/($hash{caa}+$hash{cag}) if $hash{cag}!=0;
        $raga=$hash{aga}*6/($hash{aga}+$hash{agg}+$hash{cga}+$hash{cgc}+$hash{cgg}+$hash{cgu}) if $hash{aga}!=0;
        $ragg=$hash{agg}*6/($hash{aga}+$hash{agg}+$hash{cga}+$hash{cgc}+$hash{cgg}+$hash{cgu})  if $hash{agg}!=0;
        $rcga=$hash{cga}*6/($hash{aga}+$hash{agg}+$hash{cga}+$hash{cgc}+$hash{cgg}+$hash{cgu})  if $hash{cga}!=0;
        $rcgc=$hash{cgc}*6/($hash{aga}+$hash{agg}+$hash{cga}+$hash{cgc}+$hash{cgg}+$hash{cgu}) if $hash{cgc}!=0;
        $rcgg=$hash{cgg}*6/($hash{aga}+$hash{agg}+$hash{cga}+$hash{cgc}+$hash{cgg}+$hash{cgu})  if $hash{cgg}!=0;
        $rcgu=$hash{cgu}*6/($hash{aga}+$hash{agg}+$hash{cga}+$hash{cgc}+$hash{cgg}+$hash{cgu})  if $hash{cgu}!=0;
        $ragc=$hash{agc}*6/($hash{agc}+$hash{agu}+$hash{uca}+$hash{ucc}+$hash{ucg}+$hash{ucu}) if $hash{agc}!=0;
        $ragu=$hash{agu}*6/($hash{agc}+$hash{agu}+$hash{uca}+$hash{ucc}+$hash{ucg}+$hash{ucu}) if $hash{agu}!=0;
        $ruca=$hash{uca}*6/($hash{agc}+$hash{agu}+$hash{uca}+$hash{ucc}+$hash{ucg}+$hash{ucu}) if $hash{uca}!=0;
	$rucc=$hash{ucc}*6/($hash{agc}+$hash{agu}+$hash{uca}+$hash{ucc}+$hash{ucg}+$hash{ucu}) if $hash{ucc}!=0;
        $rucg=$hash{ucg}*6/($hash{agc}+$hash{agu}+$hash{uca}+$hash{ucc}+$hash{ucg}+$hash{ucu}) if $hash{ucg}!=0;
        $rucu=$hash{ucu}*6/($hash{agc}+$hash{agu}+$hash{uca}+$hash{ucc}+$hash{ucg}+$hash{ucu}) if $hash{ucu}!=0;
        $raca=$hash{aca}*4/($hash{aca}+$hash{acc}+$hash{acg}+$hash{acu}) if $hash{aca}!=0;
        $racc=$hash{acc}*4/($hash{aca}+$hash{acc}+$hash{acg}+$hash{acu}) if $hash{acc}!=0;
        $racg=$hash{acg}*4/($hash{aca}+$hash{acc}+$hash{acg}+$hash{acu}) if $hash{acg}!=0;
        $racu=$hash{acu}*4/($hash{aca}+$hash{acc}+$hash{acg}+$hash{acu}) if $hash{acu}!=0;
        $rgua=$hash{gua}*4/($hash{gua}+$hash{guc}+$hash{gug}+$hash{guu}) if $hash{gua}!=0;
        $rguc=$hash{guc}*4/($hash{gua}+$hash{guc}+$hash{gug}+$hash{guu}) if $hash{guc}!=0;
        $rgug=$hash{gug}*4/($hash{gua}+$hash{guc}+$hash{gug}+$hash{guu}) if $hash{gug}!=0;
        $rguu=$hash{guu}*4/($hash{gua}+$hash{guc}+$hash{gug}+$hash{guu}) if $hash{guu}!=0;
        $rugg=1.0;
        $ruac=$hash{uac}*2/($hash{uac}+$hash{uau})  if $hash{uac}!=0;
        $ruau=$hash{uau}*2/($hash{uac}+$hash{uau})  if $hash{uau}!=0;
        print file2 "\t $rgca\t$rgcc\t$rgcg\t$rgcu\t$rugc\t$rugu\t$rgac\t$rgau\t$rgaa\t$rgag\t$ruuc\t$ruuu\t$rgga\t$rggc\t$rggg\t$rggu\t$rcac\t$rcau\t$raua\t$rauc\t$rauu\t$raaa\t$raag\t$ruua\t$ruug\t$rcua\t$rcuc\t$rcug\t$rcuu\t$raug\t$raac\t$raau\t$rcca\t$rccc\t$rccg\t$rccu\t$rcaa\t$rcag\t$raga\t$ragg\t$rcga\t$rcgc\t$rcgg\t$rcgu\t$ragc\t$ragu\t$ruca\t$rucc\t$rucg\t$rucu\t$raca\t$racc\t$racg\t$racu\t$rgua\t$rguc\t$rgug\t$rguu\t$rugg\t$ruac\t$ruau \n";
}
close file2;

